import { Task } from "./src/promise-tasklist.js";

console.time("total");

const task = new Task(
    new Promise((res, rej) => {
        setTimeout(() => {
            res("Yay, we got it!");
        }, 2000);
    })
);

async function test() {
    console.log(task.list);

    console.time("run");
    console.log(await task.run());
    console.timeEnd("run");
}

test();

console.timeEnd("total");
