export default class Task {
    constructor(...p) {
        if (p)
            if (p[0] instanceof Array) this.list = p[0];
            else this.list = p;
        else this.list = [];

        this.results = [];
    }

    async run() {
        for (const p of this.list) {
            this.results.push(await p);
        }

        return this.results;
    }
}
